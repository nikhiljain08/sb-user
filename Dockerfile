FROM openjdk:11
ARG JAR_FILE=build/libs/user-0.0.1.jar
COPY ${JAR_FILE} user.jar
ENTRYPOINT ["java","-jar","/user.jar"]