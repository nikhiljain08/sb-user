package com.codeon.user.controller;

import com.codeon.user.entity.*;
import com.codeon.user.exception.CustomException;
import com.codeon.user.service.ConfirmationTokenService;
import com.codeon.user.service.RefreshTokenService;
import com.codeon.user.service.UserService;
import com.codeon.user.util.EmailValidator;
import com.codeon.user.util.JwtTokenUtil;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.InvalidParameterException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService service;

    @Autowired
    ConfirmationTokenService tokenService;

    @Autowired
    RefreshTokenService refreshTokenService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private EmailValidator emailValidator;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;


    @GetMapping("/")
    public String welcome() { return "User Service Active!!"; }

    @PostMapping(value = "/register")
    public User addUser(HttpServletRequest request, @RequestBody UserRegister userRegister)
            throws CustomException {
        User user = service.addUser(userRegister);
        if(user == null) {
            throw new CustomException("User not registered", new InvalidParameterException(), HttpStatus.INTERNAL_SERVER_ERROR, request);
        }
        return user;
    }

    @GetMapping(value = "/{id}")
    public Optional<User> getUser(HttpServletRequest request, @PathVariable String id)
            throws CustomException {
        System.out.println(request.getRequestURI());
        Optional<User> user = service.getUser(id);
        if(user.isEmpty()) {
            throw new CustomException(request, HttpStatus.NOT_FOUND, "User not found");
        }
        return user;
    }

    @PostMapping(value = "/authenticate")
    public ResponseEntity<?> authenticateUser(HttpServletRequest request,
                                              @RequestBody AuthRequest authRequest)
            throws Exception {
        String username = authRequest.getUsername();
        boolean isValidEmail = emailValidator.test(username);
        if (isValidEmail) {
            username = service.loadUserNameByEmail(username);
            if(username == null || username.isEmpty()) {
                throw new CustomException(request, HttpStatus.NOT_FOUND, "User not found");
            }
        }

        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(username,
                            authRequest.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
            String jwt = jwtTokenUtil.generateToken(userDetails.getUsername());
            List<String> roles = userDetails.getAuthorities().stream()
                    .map(GrantedAuthority::getAuthority)
                    .collect(Collectors.toList());
            RefreshToken refreshToken = refreshTokenService.createRefreshToken(userDetails.getUsername());

            JwtResponse jwtResponse = JwtResponse.builder()
                    ._id(userDetails.get_id())
                    .name(userDetails.getName())
                    .email(userDetails.getEmail())
                    .username(userDetails.getUsername())
                    .roles(roles)
                    .valid(userDetails.isValid())
                    .active(userDetails.isActive())
                    .token(jwt)
                    .refreshToken(refreshToken.getToken())
                    .build();
            return ResponseEntity.ok(jwtResponse);
        }  catch (DisabledException e) {
            throw new CustomException("User Disabled", e, HttpStatus.BAD_REQUEST, request);
        } catch (BadCredentialsException e) {
            throw new CustomException("Invalid credentials", e, HttpStatus.BAD_REQUEST, request);
        } catch (ExpiredJwtException e) {
            throw new CustomException("Expired JWT", e, HttpStatus.UNAUTHORIZED, request);
        } catch (Exception e) {
            throw new CustomException("Unknown Exception", e, HttpStatus.BAD_REQUEST, request);
        }
    }

    @PostMapping("/refreshtoken")
    public ResponseEntity<?> refreshtoken(HttpServletRequest request,
                                          @RequestBody TokenRefreshRequest tokenRefreshRequest)
            throws CustomException {
        String requestRefreshToken = tokenRefreshRequest.getRefreshToken();
        RefreshToken refreshToken = refreshTokenService.findByToken(requestRefreshToken);
        if(refreshToken!=null && refreshTokenService.verifyExpiration(refreshToken)!=null) {
            String token = jwtTokenUtil.generateToken(refreshToken.getUsername());
            return ResponseEntity.ok(new TokenRefreshResponse(token, requestRefreshToken));
        }
        throw new CustomException(request, HttpStatus.INTERNAL_SERVER_ERROR, "Invalid token");
    }


    @GetMapping("/confirm")
    public String confirm(HttpServletRequest request, @RequestParam("token") String token) throws CustomException {
        return tokenService.confirmToken(request, token);
    }
}
