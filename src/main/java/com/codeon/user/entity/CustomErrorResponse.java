package com.codeon.user.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomErrorResponse {

    LocalDateTime timestamp = LocalDateTime.now();
    int status;
    String error;
    String message;
    String path;

}
