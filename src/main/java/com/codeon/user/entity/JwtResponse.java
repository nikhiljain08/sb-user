package com.codeon.user.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JwtResponse {

    @Id
    private String _id;
    private String name;
    private String username;
    private String email;
    private List<String> roles;
    private boolean valid;
    private boolean active;
    private String type = "Basic";
    private String token;
    private String refreshToken;

}
