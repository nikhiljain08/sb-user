package com.codeon.user.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "USER_TB")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @Id
    private String _id;
    private String name;
    private String username;
    private String email;
    @JsonIgnore
    private String password;
    private List<String> roles;
    private boolean valid = false;
    private boolean active = false;

}
