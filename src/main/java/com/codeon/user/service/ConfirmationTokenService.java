package com.codeon.user.service;

import com.codeon.user.entity.User;
import com.codeon.user.exception.CustomException;
import com.codeon.user.entity.ConfirmationToken;
import com.codeon.user.repository.ConfirmationTokenRepository;
import com.codeon.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class ConfirmationTokenService {

    @Autowired
    ConfirmationTokenRepository repository;

    @Autowired
    UserRepository userRepository;

    public ConfirmationToken findByToken(String token) {
        return repository.findByToken(token);
    }

    public String confirmToken(HttpServletRequest request, String token) throws CustomException {
        ConfirmationToken confirmationToken = repository.findByToken(token);

        if(confirmationToken!=null) {
            if (confirmationToken.getConfirmedAt() != null) {
                throw new CustomException(request, HttpStatus.INTERNAL_SERVER_ERROR, "Email already confirmed");
            }

            LocalDateTime expiredAt = confirmationToken.getExpiresAt();

            if (expiredAt.isBefore(LocalDateTime.now())) {
                throw new CustomException(request, HttpStatus.INTERNAL_SERVER_ERROR, "Token expired");
            }

            confirmationToken.setConfirmedAt(LocalDateTime.now());
            repository.save(confirmationToken);
            Optional<User> user = userRepository.findById(confirmationToken.getUser_id());
            if (user.isPresent()) {
                User updated_user = user.get();
                updated_user.setActive(true);
                updated_user.setValid(true);
                userRepository.save(updated_user);
            }
        }
        return "CONFIRMED";
    }

    public void saveConfirmationToken(ConfirmationToken confirmationToken) {
        repository.save(confirmationToken);
    }
}

