package com.codeon.user.service;

import com.codeon.user.entity.ConfirmationToken;
import com.codeon.user.entity.User;
import com.codeon.user.entity.UserDetailsImpl;
import com.codeon.user.entity.UserRegister;
import com.codeon.user.repository.UserRepository;
import com.codeon.user.util.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    UserRepository repository;

    @Autowired
    ConfirmationTokenService confirmationTokenService;

    @Autowired
    JwtTokenUtil jwtTokenUtil;

    public User addUser(UserRegister userRegister) {
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        User user = User.builder()
                    .name(userRegister.getName())
                .username(userRegister.getUsername())
                .email(userRegister.getEmail())
                .password(encoder.encode(userRegister.getPassword()))
                .roles(List.of("ROLE_USER"))
                .build();

        user = repository.save(user);

        String token = UUID.randomUUID().toString();
        ConfirmationToken confirmationToken =
                ConfirmationToken.builder()
                        .user_id(user.get_id())
                        .token(token)
                        .createdAt(LocalDateTime.now())
                        .expiresAt(LocalDateTime.now().plusMinutes(15))
                        .build();
        confirmationTokenService.saveConfirmationToken(confirmationToken);
        return user;
    }

    public User saveUser(User user) {
        return repository.save(user);
    }

    public Optional<User> getUser(String id) {
        return repository.findById(id);
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        User user = repository.findByUsername(username);
        return UserDetailsImpl.build(user);
    }

    public String loadUserNameByEmail(String email) {
        User user = repository.findByEmail(email);
        return user.getUsername();
    }
}
